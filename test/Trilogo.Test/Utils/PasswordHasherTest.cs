﻿using Trilogo.Api.Utils;
using Xunit;

namespace Trilogo.Test.Utils
{
    public class PasswordHasherTest
    {
        [Theory(DisplayName = "Check Password Valid")]
        [InlineData("12345678")]
        [InlineData("adkjagdas")]
        [InlineData("asçldkc,.z;x")]
        [InlineData("354sad46sad4")]
        [InlineData("!@!@#!@dsasd")]
        [InlineData("ASDAD1234145")]
        public void Password_Check_CheckedTrue(string password)
        {
            //Arrange
            var passwordHashed = PasswordHasher.HashPassword(password);

            //Act
            var result = PasswordHasher.CheckPassword(password, passwordHashed);

            //Assert
            Assert.True(result);
        }

        [Theory(DisplayName = "Check Password Inalid")]
        [InlineData("12345678", "123456788")]
        [InlineData("adkjagdas", "adkjagd")]
        [InlineData("asçldkc,.z;x", "asçldkc,z;x")]
        [InlineData("354sad46sad4", "354sad46sad5")]
        public void Password_Check_CheckedFalse(string password, string wrongPassword)
        {
            //Arrange
            var passwordHashed = PasswordHasher.HashPassword(password);

            //Act
            var result = PasswordHasher.CheckPassword(wrongPassword, passwordHashed);

            //Assert
            Assert.False(result);
        }
    }
}
