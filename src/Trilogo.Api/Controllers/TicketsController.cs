﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.ReadOnly;
using Trilogo.Api.Repository.Interfaces.WriteOnly;
using Trilogo.Api.ViewModels;
using Trilogo.Api.ViewModels.Validations;

namespace Trilogo.Api.Controllers
{
    [Route("api/tickets")]
    public class TicketsController : MainController
    {
        private readonly ITicketWriteOnlyRepository _ticketWriteOnlyRepository;
        private readonly ITicketReadOnlyRepository _ticketReadOnlyRepository;
        private readonly IMapper _mapper;

        public TicketsController(
            ITicketWriteOnlyRepository ticketWriteOnlyRepository, 
            ITicketReadOnlyRepository ticketReadOnlyRepository, 
            IMapper mapper)
        {
            _ticketWriteOnlyRepository = ticketWriteOnlyRepository;
            _ticketReadOnlyRepository = ticketReadOnlyRepository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]TicketViewModel ticketViewModel)
        {
            var validationResult = new CreateTicketValidation().Validate(ticketViewModel);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            var ticket = _mapper.Map<Ticket>(ticketViewModel);

            await _ticketWriteOnlyRepository.AddAsync(ticket);

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody]TicketViewModel ticketViewModel)
        {
            var validationResult = new UpdateTicketValidation().Validate(ticketViewModel);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            var ticket = await _ticketReadOnlyRepository.GetById(id);

            if(ticket is null)
            {
                return NotFound();
            }

            //Update ticket
            if (ticketViewModel.AuthorName != null)
            {
                ticket.AuthorName = ticketViewModel.AuthorName;
            }
            if (ticketViewModel.Date != null)
            {
                ticket.Date = ticketViewModel.Date;
            }
            ticket.Description = ticketViewModel.Description;

            await _ticketWriteOnlyRepository.UpdateAsync(ticket);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var ticket = await _ticketReadOnlyRepository.GetById(id);

            if(ticket is null)
            {
                return NotFound();
            }

            await _ticketWriteOnlyRepository.RemoveAsync(ticket);
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var ticket = await _ticketReadOnlyRepository.GetById(id);

            if (ticket is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<TicketViewModel>(ticket));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(_mapper.Map<IEnumerable<TicketViewModel>>(await _ticketReadOnlyRepository.GetAll()));
        }
    }
}