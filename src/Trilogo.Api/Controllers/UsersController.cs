﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.ReadOnly;
using Trilogo.Api.Repository.Interfaces.WriteOnly;
using Trilogo.Api.Utils;
using Trilogo.Api.ViewModels;
using Trilogo.Api.ViewModels.Validations;

namespace Trilogo.Api.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : MainController
    {
        private readonly IUserWriteOnlyRepository _userWriteOnlyRepository;
        private readonly IUserReadOnlyRepository _userReadOnlyRepository;
        private readonly IMapper _mapper;

        public UsersController(
            IUserWriteOnlyRepository userWriteOnlyRepository,
            IUserReadOnlyRepository userReadOnlyRepository,
            IMapper mapper)
        {
            _userWriteOnlyRepository = userWriteOnlyRepository;
            _userReadOnlyRepository = userReadOnlyRepository;
            _mapper = mapper;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterUserViewModel registerUserViewModel)
        {
            var validationResult = new RegisterUserValidation().Validate(registerUserViewModel);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            if (await _userReadOnlyRepository.GetByEmail(registerUserViewModel.Email) != null)
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty, "Email já está sendo utilizado"));
                return BadRequest(validationResult);
            }

            var user = _mapper.Map<User>(registerUserViewModel);

            user.Password = PasswordHasher.HashPassword(user.Password);

            await _userWriteOnlyRepository.AddAsync(user);
            return Ok();
        }
    }
}