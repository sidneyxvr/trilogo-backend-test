﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Trilogo.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class MainController : ControllerBase
    {
        protected IActionResult BadRequest(ValidationResult validationResult)
        {
            return BadRequest(validationResult.Errors.Select(error => error.ErrorMessage));
        }
    }
}