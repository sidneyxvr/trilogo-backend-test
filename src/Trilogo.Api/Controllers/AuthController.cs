﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Trilogo.Api.Extensions;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.ReadOnly;
using Trilogo.Api.Utils;
using Trilogo.Api.ViewModels;
using Trilogo.Api.ViewModels.Validations;

namespace Trilogo.Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : MainController
    {
        private readonly IUserReadOnlyRepository _userReadOnlyRepository;
        private readonly AppSettings _appSettings;

        public AuthController(
            IUserReadOnlyRepository userReadOnlyRepository,
            IOptions<AppSettings> appSettings)
        {
            _userReadOnlyRepository = userReadOnlyRepository;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            var validationResult = new LoginValidation().Validate(loginViewModel);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult);
            }

            var user = await _userReadOnlyRepository.GetByEmail(loginViewModel.Email);

            if (user is null || !PasswordHasher.CheckPassword(loginViewModel.Password, user.Password))
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty, "Usuário ou senha inválido(s)"));
                return BadRequest(validationResult);
            }

            return Ok(new { Token = GenerateJwt(user) });
        }

        private string GenerateJwt(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaim(new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()));
            claimsIdentity.AddClaim(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claimsIdentity.AddClaim(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claimsIdentity.AddClaim(new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(DateTime.UtcNow).ToString()));
            claimsIdentity.AddClaim(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64));

            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _appSettings.Emitter,
                Audience = _appSettings.ValidOn,
                Subject = claimsIdentity,
                Expires = DateTime.UtcNow.AddHours(_appSettings.HourValidation),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            return tokenHandler.WriteToken(token);
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}