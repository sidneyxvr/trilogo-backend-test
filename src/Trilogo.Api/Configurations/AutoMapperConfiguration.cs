﻿using AutoMapper;
using Trilogo.Api.Models;
using Trilogo.Api.ViewModels;

namespace Trilogo.Api.Configurations
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            CreateMap<Ticket, TicketViewModel>().ReverseMap();

            CreateMap<RegisterUserViewModel, User>();
        }
    }
}
