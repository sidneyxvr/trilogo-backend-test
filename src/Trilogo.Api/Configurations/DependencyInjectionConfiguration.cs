﻿using Microsoft.Extensions.DependencyInjection;
using Trilogo.Api.Repository.Interfaces.ReadOnly;
using Trilogo.Api.Repository.Interfaces.WriteOnly;
using Trilogo.Api.Repository.ReadOnly;
using Trilogo.Api.Repository.WriteOnly;

namespace Trilogo.Api.Configurations
{
    public static class DependencyInjectionConfiguration
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ITicketWriteOnlyRepository, TicketWriteOnlyRepository>();
            services.AddScoped<ITicketReadOnlyRepository, TicketReadOnlyRepository>();

            services.AddScoped<IUserWriteOnlyRepository, UserWriteOnlyRepository>();
            services.AddScoped<IUserReadOnlyRepository, UserReadOnlyRepository>();
        }
    }
}
