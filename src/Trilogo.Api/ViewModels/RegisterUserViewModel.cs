﻿namespace Trilogo.Api.ViewModels
{
    public class RegisterUserViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
