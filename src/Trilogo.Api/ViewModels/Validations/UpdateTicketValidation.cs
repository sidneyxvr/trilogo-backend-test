﻿using FluentValidation;
using Trilogo.Api.Models;

namespace Trilogo.Api.ViewModels.Validations
{
    public class UpdateTicketValidation : AbstractValidator<TicketViewModel>
    {
        public UpdateTicketValidation()
        {
            RuleFor(ticket => ticket.Description)
                .NotEmpty();
        }
    }
}
