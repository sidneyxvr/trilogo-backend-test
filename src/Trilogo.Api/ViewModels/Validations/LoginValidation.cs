﻿using FluentValidation;

namespace Trilogo.Api.ViewModels.Validations
{
    public class LoginValidation : AbstractValidator<LoginViewModel>
    {
        public LoginValidation()
        {
            RuleFor(user => user.Email)
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(300);

            RuleFor(user => user.Password)
                .NotEmpty()
                .Length(6, 100);
        }
    }
}
