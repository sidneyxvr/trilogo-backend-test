﻿using FluentValidation;

namespace Trilogo.Api.ViewModels.Validations
{
    public class CreateTicketValidation : AbstractValidator<TicketViewModel>
    {
        public CreateTicketValidation()
        {
            RuleFor(ticket => ticket.Description)
                .NotEmpty()
                .MaximumLength(300);

            RuleFor(ticket => ticket.AuthorName)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(ticket => ticket.Date)
                .NotEmpty();
        }
    }
}
