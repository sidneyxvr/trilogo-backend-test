﻿using FluentValidation;

namespace Trilogo.Api.ViewModels.Validations
{
    public class RegisterUserValidation : AbstractValidator<RegisterUserViewModel>
    {
        public RegisterUserValidation()
        {
            RuleFor(user => user.Email)
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(300);

            RuleFor(user => user.Name)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(user => user.Password)
                .NotEmpty()
                .Length(6, 100);
        }
    }
}
