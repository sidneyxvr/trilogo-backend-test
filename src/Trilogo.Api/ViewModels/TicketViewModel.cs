﻿using System;

namespace Trilogo.Api.ViewModels
{
    public class TicketViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AuthorName { get; set; }
        public DateTime? Date { get; set; }
    }
}
