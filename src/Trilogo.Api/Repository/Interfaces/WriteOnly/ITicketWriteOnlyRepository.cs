﻿using System.Threading.Tasks;
using Trilogo.Api.Models;

namespace Trilogo.Api.Repository.Interfaces.WriteOnly
{
    public interface ITicketWriteOnlyRepository
    {
        Task AddAsync(Ticket ticket);
        Task UpdateAsync(Ticket ticket);
        Task RemoveAsync(Ticket ticket);
    }
}
