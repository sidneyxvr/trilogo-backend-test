﻿using System.Threading.Tasks;
using Trilogo.Api.Models;

namespace Trilogo.Api.Repository.Interfaces.WriteOnly
{
    public interface IUserWriteOnlyRepository
    {
        Task AddAsync(User user);
    }
}
