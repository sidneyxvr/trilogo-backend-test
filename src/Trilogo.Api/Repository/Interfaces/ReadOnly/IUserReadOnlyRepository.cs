﻿using System.Threading.Tasks;
using Trilogo.Api.Models;

namespace Trilogo.Api.Repository.Interfaces.ReadOnly
{
    public interface IUserReadOnlyRepository
    {
        Task<User> GetByEmail(string email);
    }
}
