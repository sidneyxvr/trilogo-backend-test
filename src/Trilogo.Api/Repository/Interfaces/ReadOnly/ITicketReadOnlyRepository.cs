﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Trilogo.Api.Models;

namespace Trilogo.Api.Repository.Interfaces.ReadOnly
{
    public interface ITicketReadOnlyRepository
    {
        Task<Ticket> GetById(int id);
        Task<IEnumerable<Ticket>> GetAll();
    }
}
