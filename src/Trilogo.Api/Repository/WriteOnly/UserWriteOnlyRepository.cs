﻿using System.Threading.Tasks;
using Trilogo.Api.Data;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.WriteOnly;

namespace Trilogo.Api.Repository.WriteOnly
{
    public class UserWriteOnlyRepository : IUserWriteOnlyRepository
    {
        private readonly TrilogoContext _context;

        public UserWriteOnlyRepository(TrilogoContext context)
        {
            _context = context;
        }

        public async Task AddAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }
    }
}