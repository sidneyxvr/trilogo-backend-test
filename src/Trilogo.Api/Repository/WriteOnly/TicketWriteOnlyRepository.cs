﻿using System.Threading.Tasks;
using Trilogo.Api.Data;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.WriteOnly;

namespace Trilogo.Api.Repository.WriteOnly
{
    public class TicketWriteOnlyRepository : ITicketWriteOnlyRepository
    {
        private readonly TrilogoContext _context;

        public TicketWriteOnlyRepository(TrilogoContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Ticket ticket)
        {
            await _context.Tickets.AddAsync(ticket);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(Ticket ticket)
        {
            _context.Tickets.Remove(ticket);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Ticket ticket)
        {
            _context.Tickets.Update(ticket);
            await _context.SaveChangesAsync();
        }
    }
}
