﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.ReadOnly;

namespace Trilogo.Api.Repository.ReadOnly
{
    public class TicketReadOnlyRepository : ITicketReadOnlyRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public TicketReadOnlyRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<IEnumerable<Ticket>> GetAll()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlStatement = "SELECT Id, Description, AuthorName, Date FROM Tickets";

                return await connection.QueryAsync<Ticket>(sqlStatement);
            }
        }

        public async Task<Ticket> GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlStatement = "SELECT Id, Description, AuthorName, Date FROM Tickets WHERE Id = @Id";

                return await connection.QueryFirstOrDefaultAsync<Ticket>(sqlStatement, new { id });
            }
        }
    }
}
