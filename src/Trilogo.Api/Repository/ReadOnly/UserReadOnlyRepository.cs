﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Trilogo.Api.Models;
using Trilogo.Api.Repository.Interfaces.ReadOnly;

namespace Trilogo.Api.Repository.ReadOnly
{
    public class UserReadOnlyRepository : IUserReadOnlyRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public UserReadOnlyRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<User> GetByEmail(string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                var sqlStatement = "SELECT Id, Name, Email, Password FROM Users WHERE Email = @Email";

                return await connection.QueryFirstOrDefaultAsync<User>(sqlStatement, new { email });
            }
        }
    }
}