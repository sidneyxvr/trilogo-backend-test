﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Trilogo.Api.Models;

namespace Trilogo.Api.Data.Mappings
{
    public class TicketMapping : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.Property(ticket => ticket.Description)
                .IsRequired()
                .HasColumnType("varchar(300)");

            builder.Property(ticket => ticket.AuthorName)
                .HasColumnType("varchar(100)");
        }
    }
}
