﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Trilogo.Api.Models;

namespace Trilogo.Api.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.Email)
                .IsRequired()
                .HasColumnType("varchar(300)");

            builder.Property(user => user.Name)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(user => user.Password)
                .IsRequired()
                .HasColumnType("varchar(100)");
        }
    }
}
