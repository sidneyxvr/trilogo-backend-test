﻿using Microsoft.EntityFrameworkCore;
using Trilogo.Api.Models;

namespace Trilogo.Api.Data
{
    public class TrilogoContext : DbContext
    {
        public TrilogoContext(DbContextOptions<TrilogoContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TrilogoContext).Assembly);
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
    }
}