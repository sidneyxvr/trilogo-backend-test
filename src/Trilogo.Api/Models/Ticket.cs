﻿using System;

namespace Trilogo.Api.Models
{
    public class Ticket : Entity
    {
        public string Description { get; set; }
        public string AuthorName { get; set; }
        public DateTime? Date { get; set; }
    }
}
